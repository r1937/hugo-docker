# hugo-docker

Dockerfile to build latest Hugo image

## What is Hugo

[Hugo](https://gohugo.io/) is one of the most popular open-source static site generators. With its amazing speed and flexibility, Hugo makes building websites fun again.

## Getting started

## Usage

### Update Hugo Version

In the `.gitlab-ci.yml` file change `HUGOV` to the Hugo version you need.

Once the pipeline succeeds you can reference the image with

```
registry.gitlab.com/r1937/hugo-docker/hugo:<hugo-version>
```


### Local use

If you'd like to use this image locally then build the image with

```
docker build --build-arg VERSION=<hugo-version> -t hugo:<hugo-version> .
```

Now you can use the Hugo CLI with the following format:

```
docker run --rm -it hugo:0.91.2 hugo <hugo-command>
```

You can also reference the version with latest `hugo:latest`.

#### Examples:

Hugo help:
```
docker run --rm -it hugo:0.91.2 hugo help
```

Hugo serve:
```
docker run --rm -it -v $PWD:/src -p 1313:1313 -u hugo hugo:0.91.2 hugo server -w --bind=0.0.0.0
```
The example above maps your local directory to the working directory within the container and binds the exposed container port to make it accessible from the host machine.

Hugo build:
```
docker run --rm -it -v $PWD:/src -u hugo hugo:0.91.2 hugo
```
If you need to run these commands from another directory make sure to change `$PWD` to the path of the directory contianing your Hugo project.